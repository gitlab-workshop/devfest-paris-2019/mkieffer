package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote

import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.Persona
import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.PersonaRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/quotes")
class QuoteController(val quoteRepository: QuoteRepository, val personaRepository: PersonaRepository) {

    @GetMapping
    fun findAll(): List<Quote> = quoteRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable id: Long) = quoteRepository.findByIdOrNull(id)

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    fun save(@RequestBody payload: QuotePayload): Quote {

        val author = personaRepository
                .findFirstByFirstNameAndLastName(payload.author.firstName, payload.author.lastName)
                ?: personaRepository.save(Persona(null, payload.author.firstName, payload.author.lastName))

        return quoteRepository.save(Quote(null, author, payload.body))
    }
}

class QuotePayload(val body: String, val author: AuthorPayload)
class AuthorPayload(val firstName: String, val lastName: String)
